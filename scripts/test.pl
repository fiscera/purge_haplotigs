#!/usr/bin/env perl

# Copyright (c) 2017 Michael Roach (Australian Wine Research Institute)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

use strict;
use warnings;
use FindBin qw($RealBin);
use File::Copy qw(copy);


# setup
my $TMP_DIR = 'tmp_purge_haplotigs';
my $TEST_DIR = "$TMP_DIR/TEST";

if (!(-d $TMP_DIR)){
    mkdir $TMP_DIR;
}

if (!(-d $TEST_DIR)){
    mkdir $TEST_DIR;
}


# copy test files
for my $file (qw/aligned.bam contigs.fa repeats.bed Makefile validate.md5/){
    copy "$RealBin/../test/$file", "$TEST_DIR/$file";
}


# cleanup any old test output files
cleanup();


# run the test
if (system("cd $TEST_DIR && make test") == 0){
    print STDERR "\nALL TESTS PASSED\n\n";
} else {
    print STDERR "\nONE OR MORE TESTS FAILED\n\n";
    exit(1);
}


# cleanup
cleanup();

exit(0);


sub cleanup {
    if (system("cd $TEST_DIR && make clean") != 0){
        print STDERR "\nError cleaning up previous test files\n";
        exit(1);
    }
    return;
}


