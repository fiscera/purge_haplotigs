exit; 
# If you're testing your installation of Purge Haplotigs then use the makefile
make test # to validate
make clean # to clean up

# Validation and test dataset
# The test dataset is from here: https://doi.org/10.5281/zenodo.1042847


# The test commands:
purge_haplotigs readhist aligned.bam

purge_haplotigs contigcov -i aligned.bam.genecov -l 3 -m 20 -h 70

purge_haplotigs purge -g contigs.fa -c coverage_stats.csv -b aligned.bam -t 16

purge_haplotigs purge -g contigs.fa -c coverage_stats.csv -b aligned.bam -t $(THREADS) -o curated.WM -windowmasker

purge_haplotigs ncbiplace -p curated.fasta -h curated.haplotigs.fasta -t $(THREADS)

purge_haplotigs ncbiplace -p curated.WM.fasta -h curated.WM.haplotigs.fasta -t $(THREADS) -windowmasker -o ncbi_placements.WM.tsv

md5sum -c validate.md5

md5sum -c validate.WM.md5
